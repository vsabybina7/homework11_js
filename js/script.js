// Теоретический вопрос
//
// Почему для работы с input не рекомендуется использовать события клавиатуры?
// Ответ: События клавиатуры предназначены именно для работы с клавиатурой, в input может использоватся, но если текст будет вставлен с помощью миши, тогда события клавишь ни к чему!
//
//     Задание
//     Реализовать функцию подсветки нажимаемых клавиш
//
// Технические требования:
//
//     В файле index.html лежит разметка для кнопок.
//     Каждая кнопка содержит в себе название клавиши на клавиатуре
// По нажатию указанных клавиш - та кнопка, на которой написана эта буква, должна окрашиваться в синий цвет. При этом, если какая-то другая буква уже ранее была окрашена в синий цвет - она становится черной. Например по нажатию Enter первая кнопка окрашивается в синий цвет. Далее, пользователь нажимает S, и кнопка S окрашивается в синий цвет, а кнопка Enter опять становится черной.

    let btns = document.querySelectorAll('.btn')
        btnsLength = btns.length;
        console.log(btns);

    document.addEventListener('keydown', (event) => {
        for (let i = 0; i < btnsLength; i++) {
            if (event.code === btns[i].dataset.set) {
                console.log(event);
                console.log(btns[i].dataset.set);
                btns[i].style.backgroundColor = 'blue'
            } else if (event.code !== btns[i].dataset.set) {
                btns[i].style.backgroundColor = 'black'
            }
        }
    })




